/**************************
 *Jalen Barnes
 *CPSC 1021,001, F20
 *jalen5@g.clemson.edu
 *Nushrat Humaria
 **************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
srand(unsigned (time(0)));

	//srand(time(0));
	//random_shuffle(arr,arr+2,myrandom);

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee array[10];
	employee *ptr, *endPtr; //*functPtr;
	ptr = &array[0];
	endPtr = &array[11];

	//functPtr = myrandom;

// This for loop intializes the original array of 10 employees.
	for(int i = 0; i < 10; i++ )
	{
		cout << "Please enter the employees last name: \n"<< endl;
		cin >> array[i].lastName;
		cout << "Please enter the employees first name: \n"<< endl;
		cin >> array[i].firstName;
		cout << "Please enter the employees year of birth: \n"<< endl;
		cin >> array[i].birthYear;
		cout << "Please enter the employees hourly wage: \n" << endl;
		cin >> array[i].hourlyWage;

	}
  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	 random_shuffle(&ptr, &endPtr, myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/

		employee newArray[5];
		// initialzed smaller array and set it to the the first 5 values
		for (int i = 0; i < 5; i++)
		{
			 newArray[i] = array[i];
		}
		// pointer variables to pass through the sort function.
		employee *newPtr, *newEndPtr;
		newPtr = &newArray[0];
		newEndPtr = &newArray[4];
    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		// lhs.lastName < rhs.lastName
			//suit_order();
			sort(newPtr, newEndPtr, name_order);
    /*Now print the array below */
		for (auto x: newArray )
		{// this will print out the randomized newArray.
			cout<<setw(10) << right << x.lastName << "," << x.firstName << "\n" << endl;
			cout << setw(10) << right << x.birthYear << "\n" << endl;
			cout << fixed << showpoint << setprecision(2);
			cout <<setw(10) <<  right  << x.hourlyWage << "\n" << endl;
		}



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs)
 {

	return lhs.lastName < rhs.lastName;

}
